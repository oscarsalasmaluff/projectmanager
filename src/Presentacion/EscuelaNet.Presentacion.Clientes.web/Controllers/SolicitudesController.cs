﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class SolicitudesController : Controller
    {
        
        public ActionResult Index(int idCliente, int idUnidad)
        {
            var cliente = Contexto.Instancia.Clientes[idCliente];
            var unidad = cliente.Unidades[idUnidad];

            if (unidad.Solicitudes == null)
            {
                TempData["error"] = "Unidad sin solicitudes";
                return RedirectToAction("../unidades/Index/"+idCliente);
            }

            var solicitudes = unidad.Solicitudes;

            List<Solicitud> solic = new List<Solicitud>();                      
            
            foreach (var solicitud in solicitudes)
            {
                solic.Add(solicitud);
            }
            
            var model = new SolicitudesIndexModel()
            {
                Titulo = "Solicitudes de la Unidad '"+unidad.RazonSocial+ "' del Cliente '"+cliente.RazonSocial+"'",
                IdCliente = idCliente,
                IdUnidad = idUnidad,
                Solicitudes = solic
            };

            return View(model);
        }


        public ActionResult New(int idCliente, int idUnidad)
        {
            var cliente = Contexto.Instancia.Clientes[idCliente];
            var unidad = cliente.Unidades[idUnidad];
            var model = new NuevaSolicitudModel() {
                Titul= "Nueva Solicitud para la Unidad '"+unidad.RazonSocial+"' del Cliente '"+cliente.RazonSocial+"'",
                IdC = idCliente,
                IdU = idUnidad
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].AgregarSolicitud(new Solicitud(model.Titulo, model.Descripcion));
                    TempData["success"] = "Solicitud creada";                   
                    return RedirectToAction("Index", new { idCliente = model.IdC, idUnidad = model.IdU });

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int idCliente, int idUnidad, int idSolic)
        {
            var cantidad = Contexto.Instancia.Clientes.Count();
            if (idCliente < 0 || idCliente >= cantidad)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index", new { idCliente =idCliente, idUnidad = idUnidad });
            }
            else
            {
                var cliente = Contexto.Instancia.Clientes[idCliente];
                var unidad = cliente.Unidades[idUnidad];
                var solicitud = unidad.Solicitudes[idSolic];

                var model = new NuevaSolicitudModel()
                {
                    Titul = "Editar la Solicitud '"+solicitud.Titulo+"' de la Unidad '" + unidad.RazonSocial + "' del Cliente '" + cliente.RazonSocial + "'",
                    IdC = idCliente,
                    IdU = idUnidad,
                    IdS = idSolic,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].Solicitudes[model.IdS].Titulo = model.Titulo;
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].Solicitudes[model.IdS].Descripcion = model.Descripcion;
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].Solicitudes[model.IdS].CambiarEstado( model.Estado);

                    TempData["success"] = "Solicitud editada";
                    return RedirectToAction("Index", new { idCliente = model.IdC, idUnidad = model.IdU });
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int idCliente, int idUnidad, int idSolic)
        {
            var cantidad = Contexto.Instancia.Clientes.Count();
            if (idCliente < 0 || idCliente >= cantidad)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index", new { idCliente = idCliente, idUnidad = idUnidad });
            }
            else
            {
                var cliente = Contexto.Instancia.Clientes[idCliente];
                var unidad = cliente.Unidades[idUnidad];
                var solicitud = unidad.Solicitudes[idSolic];

                var model = new NuevaSolicitudModel()
                {
                    IdC = idCliente,
                    IdU = idUnidad,
                    IdS = idSolic,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaSolicitudModel model)
        {

            try
            {
                Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].Solicitudes.RemoveAt(model.IdS);
                TempData["success"] = "Solicitud borrada";
                return RedirectToAction("Index", new { idCliente = model.IdC, idUnidad = model.IdU });
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}