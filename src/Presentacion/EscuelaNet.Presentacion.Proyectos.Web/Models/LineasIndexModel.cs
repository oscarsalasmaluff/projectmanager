﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class LineasIndexModel
    {
        public string Titulo { get; set; }
        public List<LineaDeProduccion> LineasDeProduccion { get; set; }
    }
}