﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Proyectos;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class NuevoProyectoModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string NombreResponsable { get; set; }
        public long TelefonoResponsable { get; set; }
        public string EmailResponsable { get; set; }
        public int IDLinea { get; set; }
    }
}