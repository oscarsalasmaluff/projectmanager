﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class EquiposController : Controller
    {
        // GET: Equipos
        public ActionResult Index()
        {
            var Equipo = Contexto.Instancia.Equipos;

            var model = new EquiposIndexModel()
            {
                Titulo = "Prueba de Equipos",
                Equipos = Equipo
            };
            return View(model);
        }
        public ActionResult New()
        {
            var model = new NuevoEquipoModel();
            return View(model);

        }
        [HttpPost]
        public ActionResult New(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Equipos.Add(new Equipo(model.Nombre, model.Pais, model.HusoHorario));
                    
                    TempData["success"] = "Equipo Creado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Edit(int id)
        {
            var equipo = Contexto.Instancia.Equipos[id];
            var model = new NuevoEquipoModel()
            {
                Nombre = equipo.Nombre,
                Pais = equipo.Pais,
                HusoHorario = equipo.HusoHorario,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Equipos[model.Id].Nombre = model.Nombre;
                    Contexto.Instancia.Equipos[model.Id].Pais = model.Pais;
                    Contexto.Instancia.Equipos[model.Id].HusoHorario = model.HusoHorario;
                    TempData["success"] = "Equipo Editado Correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        public ActionResult Delete(int id)
        {
            var equipo = Contexto.Instancia.Equipos[id];
            var model = new NuevoEquipoModel()
            {
                Nombre = equipo.Nombre,
                Pais = equipo.Pais,
                HusoHorario = equipo.HusoHorario,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevoEquipoModel model)
        {

            try
            {
                Contexto.Instancia.Equipos.RemoveAt(model.Id);
                TempData["success"] = "Equipo Borrado Correctamente";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }



        }

    }
}