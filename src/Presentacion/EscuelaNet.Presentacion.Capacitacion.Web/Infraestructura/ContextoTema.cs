﻿using System;
using EscuelaNet.Dominio.Capacitaciones; 
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura
{
    public sealed class ContextoTema
    {
        private static ContextoTema _instancia = new ContextoTema();
        public List<Tema> Temas { get; set; }

        private ContextoTema()
        {

            this.Temas = new List<Tema>();

            this.Temas.Add(new Tema("MVC",NivelTema.Basico));
            this.Temas.Add(new Tema(".Net Core", NivelTema.Intermedio));
            this.Temas.Add(new Tema("Java", NivelTema.Avanzado));

        }

        public static ContextoTema Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}