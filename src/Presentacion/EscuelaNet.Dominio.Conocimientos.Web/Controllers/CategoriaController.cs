﻿
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Dominio.Conocimientos.Web.Infraestructura;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Controllers
{
    public class CategoriaController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            var Categoria = Contexto.Instancia.Categorias;
            var model = new CategoriaIndexModel()
            {
                Titulo = "Primera prueba",
                Categorias = Categoria
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaCategoriaModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaCategoriaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Categorias.Add(new Categoria(model.Id, model.Nombre, model.Descripcion));

                    TempData["success"] = "Nueva categoria creada creada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }
        }

        public ActionResult Edit(int id)
        {
            var linea = Contexto.Instancia.Categorias[id];
            var model = new NuevaCategoriaModel()
            {
                Nombre = linea.Nombre,
                Id = id,
                Descripcion = linea.Descripcion
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(NuevaCategoriaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Categorias[model.Id].Nombre
                        = model.Nombre;
                    TempData["success"] = "Linea de producción editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var linea = Contexto.Instancia.Categorias[id];
            var model = new NuevaCategoriaModel()
            {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevaCategoriaModel model)
        {
            try
            {
                Contexto.Instancia.Categorias.RemoveAt(model.Id);
                TempData["success"] = "Linea de producción borrada";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}