﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace EscuelaNet.Dominio.Conocimientos.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();

        public List<Categoria> Categorias { set; get; }

        private Contexto()
        {
            this.Categorias = new List<Categoria>();

            Categorias.Add(new Categoria(1,"Esto es una descripcion 1","Algo"));
            Categorias.Add(new Categoria(2,"Esto es una descripcion 2", "Algo2"));
            Categorias.Add(new Categoria(3,"Esto es una descripcion 3", "Algo3"));
        }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }

        }

    }
}