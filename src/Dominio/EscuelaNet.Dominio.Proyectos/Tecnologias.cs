﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class Tecnologias
    {
        public string nombre { get; set; }

        public Tecnologias(string nombre)
        {
            this.nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
        }
    }
}
